function openTab(e, tabName) {
    let i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("thanks-content");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tab-menu-item");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "flex";
    e.currentTarget.className += " active";
  }